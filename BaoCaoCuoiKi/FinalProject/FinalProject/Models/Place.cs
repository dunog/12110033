﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FinalProject.Models
{
    public class Place
    {
        public int ID { set; get; }
        public DateTime DateCreated { set; get; }
        public DateTime DateUpdated { set; get; }
        public string PlaceName { set; get; }
        public string SpecialFood { set; get; }
        public string PlaceAdress { set; get; }
        public string PhoneNumber { set; get; }
        public string Website { set; get; }
        public DateTime OpenTime { set; get; }
        public DateTime CloseTime { get; set; }
        public bool isWifi { set; get; }
        public string MenuImage { set; get; }
        public string Description { set; get; }
        public int ViewQuantity { set; get; }
        public int LikeQuantity { set; get; }

        //n
        public virtual District District { set; get; }
        public int DistrictID { set; get; }

        //n 
        public virtual Activity Activity { set; get; }
        public int ActivityID { set; get; }
        //n
        public virtual PlaceType PlaceType { set; get; }
        public int PlaceTypeID { set; get; }
        //n
        public virtual UserProfile UserProfile { set; get; }
        public int UserId { set; get; }
        //1
        public virtual ICollection<Vote> Votes { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<PlaceImage> PlaceImages { set; get; }




    }
}