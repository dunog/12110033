﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FinalProject.Models
{
    public class PlaceImage
    {
        public int ID {set; get;}
        public string PlaceImageLink { set; get; }

        public int PlaceID { set; get; }
        public virtual Place Place { set; get; }
    }
}