﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FinalProject.Models
{
    public class Comment
    {
        public int ID { set; get; }
        [Required]
        public string Body { set; get; }
        [Required]
        public DateTime DateCreated { set; get; }
        [Required]
        public DateTime DateUpdated { set; get; }
        public int UserId { set; get; }

        public virtual Place Place { set; get; }
        public int PlaceID { set; get; }

    }
}