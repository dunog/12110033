﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FinalProject.Models
{
    public class Vote
    {
        public int ID { set; get; }
        [Required]
        [Range(0, 100, ErrorMessage="Giá trị của Vote phải từ 0 đến 100")]
        public int PlaceVote { set; get; }
        [Required]
        [Range(0, 100, ErrorMessage = "Giá trị của Vote phải từ 0 đến 100")]
        public int ViewVote { set; get; }
        [Required]
        [Range(0, 100, ErrorMessage = "Giá trị của Vote phải từ 0 đến 100")]
        public int QualityVote { set; get; }
        [Required]
        [Range(0, 100, ErrorMessage = "Giá trị của Vote phải từ 0 đến 100")]
        public int ServiceVote { set; get; }
        [Required]
        [Range(0, 100, ErrorMessage = "Giá trị của Vote phải từ 0 đến 100")]
        public int CostVote { set; get; }

        public int UserId { set; get; }


        public virtual Place Place { set; get; }
        public int PlaceID { set; get; }
    }
}