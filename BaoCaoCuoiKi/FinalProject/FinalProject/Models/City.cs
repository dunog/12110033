﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FinalProject.Models
{
    public class City
    {
        public int ID { set; get; }
        [Required]
        public string CityName { set; get; }
        public ICollection<District> Districts { set; get; }
    }
}