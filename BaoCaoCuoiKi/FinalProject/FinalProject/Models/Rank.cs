﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FinalProject.Models
{
    public class Rank
    {
        public int ID { set; get; }
        [Required]
        public string RankName { get; set; }
        [Required]
        public int MinValue { set; get; }
        [Required]
        public int MaxValue { set; get; }

        public ICollection<UserProfile> UserProfiles { set; get; }
    }
}