﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FinalProject.Models
{
    public class Activity
    {
        public int ID { set; get; }
        [Required]
        public string ActivityName { set; get; }
        public virtual ICollection<Place> Places { set; get; }
    }
}