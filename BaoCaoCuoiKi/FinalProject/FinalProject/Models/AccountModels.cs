﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;

namespace FinalProject.Models
{
    public class UsersContext : DbContext
    {
        public UsersContext()
            : base("DefaultConnection")
        {
        }

        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<City> Cities { set; get; }
        public DbSet<District> Districts { set; get; }
        public DbSet<Place> Places { set; get; }
        public DbSet<Activity> Activities { set; get; }
        public DbSet<PlaceType> PlaceTypes { set; get; }
        public DbSet<Comment> Comments { set; get; }
        public DbSet<Tag> Tags { set; get; }
        public DbSet<Vote> Votes { set; get; }
        public DbSet<Rank> Ranks { set; get; }
        public DbSet<PlaceImage> PlaceImages { set; get; }
        //Tạo quan hệ nhiều nhiều
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Place>()
                .HasMany(s => s.Tags).WithMany(p => p.Place)
                .Map(d => d.MapLeftKey("PostID").MapRightKey("TagID").ToTable("PostTag"));
        }
    }

    [Table("UserProfile")]
    public class UserProfile
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }
        public string UserName { get; set; }
        public DateTime Birthday { set; get; }
        public string Avatar { set; get; }
        public int CumulativeScore { set; get; }

        public virtual Rank Rank { set; get; }
        public int RankID { set; get; }
        public virtual ICollection<Place> Places { set; get; }
    }

    public class RegisterExternalLoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        public string ExternalLoginData { get; set; }
    }

    public class LocalPasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Độ dài {0} phải ít nhất là {2} kí tự.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "Mật khẩu và mật khẩu nhập lại không đúng.")]
        public string ConfirmPassword { get; set; }
    }

    public class LoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Độ dài {0} phải ít nhất là {2} kí tự.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "Mật khẩu và mật khẩu nhập lại không đúng.")]
        public string ConfirmPassword { get; set; }
        public DateTime Birthday { set; get; }

    }

    public class ExternalLogin
    {
        public string Provider { get; set; }
        public string ProviderDisplayName { get; set; }
        public string ProviderUserId { get; set; }
    }
}
