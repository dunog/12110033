﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FinalProject.Models
{
    public class District
    {
        public int ID { set; get; }
        [Required]
        public string DistrictName { set; get; }
        public virtual ICollection<Place> Places { set; get; }

        //A district belongs to a city
        public virtual City City { set; get; }
        public int CityID { set; get; }

    }
}