﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FinalProject.Models
{
    public class Tag
    {
        public int ID { set; get; }
        [Required]
        [MaxLength(2000, ErrorMessage="Độ dài nội dung không được quá 2000 kí tự")]
        [MinLength(10, ErrorMessage="Độ dài nội dung phải lớn hơn 10 kí tự")]
        public string Content { set; get; }
        public virtual ICollection<Place> Place { set; get; }
    }
}