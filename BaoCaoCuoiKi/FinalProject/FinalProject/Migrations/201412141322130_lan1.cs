namespace FinalProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserProfile",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                        Birthday = c.DateTime(nullable: false),
                        Avatar = c.String(),
                        CumulativeScore = c.Int(nullable: false),
                        RankID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.Ranks", t => t.RankID, cascadeDelete: true)
                .Index(t => t.RankID);
            
            CreateTable(
                "dbo.Ranks",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        RankName = c.String(nullable: false),
                        MinValue = c.Int(nullable: false),
                        MaxValue = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Places",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        DateCreated = c.DateTime(nullable: false),
                        DateUpdated = c.DateTime(nullable: false),
                        PlaceName = c.String(),
                        SpecialFood = c.String(),
                        PlaceAdress = c.String(),
                        PhoneNumber = c.String(),
                        Website = c.String(),
                        OpenTime = c.DateTime(nullable: false),
                        CloseTime = c.DateTime(nullable: false),
                        isWifi = c.Boolean(nullable: false),
                        MenuImage = c.String(),
                        Description = c.String(),
                        ViewQuantity = c.Int(nullable: false),
                        LikeQuantity = c.Int(nullable: false),
                        DistrictID = c.Int(nullable: false),
                        ActivityID = c.Int(nullable: false),
                        PlaceTypeID = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Districts", t => t.DistrictID, cascadeDelete: true)
                .ForeignKey("dbo.Activities", t => t.ActivityID, cascadeDelete: true)
                .ForeignKey("dbo.PlaceTypes", t => t.PlaceTypeID, cascadeDelete: true)
                .ForeignKey("dbo.UserProfile", t => t.UserId, cascadeDelete: true)
                .Index(t => t.DistrictID)
                .Index(t => t.ActivityID)
                .Index(t => t.PlaceTypeID)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Districts",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        DistrictName = c.String(nullable: false),
                        CityID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Cities", t => t.CityID, cascadeDelete: true)
                .Index(t => t.CityID);
            
            CreateTable(
                "dbo.Cities",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CityName = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Activities",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ActivityName = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.PlaceTypes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        PlaceTypeName = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Votes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        PlaceVote = c.Int(nullable: false),
                        ViewVote = c.Int(nullable: false),
                        QualityVote = c.Int(nullable: false),
                        ServiceVote = c.Int(nullable: false),
                        CostVote = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        PlaceID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Places", t => t.PlaceID, cascadeDelete: true)
                .Index(t => t.PlaceID);
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Content = c.String(nullable: false, maxLength: 2000),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Body = c.String(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateUpdated = c.DateTime(nullable: false),
                        UserId = c.Int(nullable: false),
                        PlaceID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Places", t => t.PlaceID, cascadeDelete: true)
                .Index(t => t.PlaceID);
            
            CreateTable(
                "dbo.PlaceImages",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        PlaceImageLink = c.String(),
                        PlaceID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Places", t => t.PlaceID, cascadeDelete: true)
                .Index(t => t.PlaceID);
            
            CreateTable(
                "dbo.PostTag",
                c => new
                    {
                        PostID = c.Int(nullable: false),
                        TagID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PostID, t.TagID })
                .ForeignKey("dbo.Places", t => t.PostID, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.TagID, cascadeDelete: true)
                .Index(t => t.PostID)
                .Index(t => t.TagID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.PostTag", new[] { "TagID" });
            DropIndex("dbo.PostTag", new[] { "PostID" });
            DropIndex("dbo.PlaceImages", new[] { "PlaceID" });
            DropIndex("dbo.Comments", new[] { "PlaceID" });
            DropIndex("dbo.Votes", new[] { "PlaceID" });
            DropIndex("dbo.Districts", new[] { "CityID" });
            DropIndex("dbo.Places", new[] { "UserId" });
            DropIndex("dbo.Places", new[] { "PlaceTypeID" });
            DropIndex("dbo.Places", new[] { "ActivityID" });
            DropIndex("dbo.Places", new[] { "DistrictID" });
            DropIndex("dbo.UserProfile", new[] { "RankID" });
            DropForeignKey("dbo.PostTag", "TagID", "dbo.Tags");
            DropForeignKey("dbo.PostTag", "PostID", "dbo.Places");
            DropForeignKey("dbo.PlaceImages", "PlaceID", "dbo.Places");
            DropForeignKey("dbo.Comments", "PlaceID", "dbo.Places");
            DropForeignKey("dbo.Votes", "PlaceID", "dbo.Places");
            DropForeignKey("dbo.Districts", "CityID", "dbo.Cities");
            DropForeignKey("dbo.Places", "UserId", "dbo.UserProfile");
            DropForeignKey("dbo.Places", "PlaceTypeID", "dbo.PlaceTypes");
            DropForeignKey("dbo.Places", "ActivityID", "dbo.Activities");
            DropForeignKey("dbo.Places", "DistrictID", "dbo.Districts");
            DropForeignKey("dbo.UserProfile", "RankID", "dbo.Ranks");
            DropTable("dbo.PostTag");
            DropTable("dbo.PlaceImages");
            DropTable("dbo.Comments");
            DropTable("dbo.Tags");
            DropTable("dbo.Votes");
            DropTable("dbo.PlaceTypes");
            DropTable("dbo.Activities");
            DropTable("dbo.Cities");
            DropTable("dbo.Districts");
            DropTable("dbo.Places");
            DropTable("dbo.Ranks");
            DropTable("dbo.UserProfile");
        }
    }
}
