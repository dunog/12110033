﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FinalProject.Models;

namespace FinalProject.Controllers
{
    public class VotesController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /Votes/

        public ActionResult Index()
        {
            var votes = db.Votes.Include(v => v.Place);
            return View(votes.ToList());
        }

        //
        // GET: /Votes/Details/5

        public ActionResult Details(int id = 0)
        {
            Vote vote = db.Votes.Find(id);
            if (vote == null)
            {
                return HttpNotFound();
            }
            return View(vote);
        }

        //
        // GET: /Votes/Create

        public ActionResult Create()
        {
            ViewBag.PlaceID = new SelectList(db.Places, "ID", "PlaceName");
            return View();
        }

        //
        // POST: /Votes/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Vote vote)
        {
            if (ModelState.IsValid)
            {
                db.Votes.Add(vote);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PlaceID = new SelectList(db.Places, "ID", "PlaceName", vote.PlaceID);
            return View(vote);
        }

        //
        // GET: /Votes/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Vote vote = db.Votes.Find(id);
            if (vote == null)
            {
                return HttpNotFound();
            }
            ViewBag.PlaceID = new SelectList(db.Places, "ID", "PlaceName", vote.PlaceID);
            return View(vote);
        }

        //
        // POST: /Votes/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Vote vote)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vote).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PlaceID = new SelectList(db.Places, "ID", "PlaceName", vote.PlaceID);
            return View(vote);
        }

        //
        // GET: /Votes/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Vote vote = db.Votes.Find(id);
            if (vote == null)
            {
                return HttpNotFound();
            }
            return View(vote);
        }

        //
        // POST: /Votes/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Vote vote = db.Votes.Find(id);
            db.Votes.Remove(vote);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}