﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FinalProject.Models;

namespace FinalProject.Controllers
{
    public class RanksController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /Ranks/

        public ActionResult Index()
        {
            return View(db.Ranks.ToList());
        }

        //
        // GET: /Ranks/Details/5

        public ActionResult Details(int id = 0)
        {
            Rank rank = db.Ranks.Find(id);
            if (rank == null)
            {
                return HttpNotFound();
            }
            return View(rank);
        }

        //
        // GET: /Ranks/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Ranks/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Rank rank)
        {
            if (ModelState.IsValid)
            {
                db.Ranks.Add(rank);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(rank);
        }

        //
        // GET: /Ranks/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Rank rank = db.Ranks.Find(id);
            if (rank == null)
            {
                return HttpNotFound();
            }
            return View(rank);
        }

        //
        // POST: /Ranks/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Rank rank)
        {
            if (ModelState.IsValid)
            {
                db.Entry(rank).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(rank);
        }

        //
        // GET: /Ranks/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Rank rank = db.Ranks.Find(id);
            if (rank == null)
            {
                return HttpNotFound();
            }
            return View(rank);
        }

        //
        // POST: /Ranks/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Rank rank = db.Ranks.Find(id);
            db.Ranks.Remove(rank);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}