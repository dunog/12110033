﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FinalProject.Models;

namespace FinalProject.Controllers
{
    public class PlacesController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /Places/

        public ActionResult Index()
        {
            var places = db.Places.Include(p => p.District).Include(p => p.Activity).Include(p => p.PlaceType).Include(p => p.UserProfile);
            return View(places.ToList());
        }

        //
        // GET: /Places/Details/5

        public ActionResult Details(int id = 0)
        {
            Place place = db.Places.Find(id);
            if (place == null)
            {
                return HttpNotFound();
            }
            return View(place);
        }

        //
        // GET: /Places/Create

        public ActionResult Create()
        {
            ViewBag.DistrictID = new SelectList(db.Districts, "ID", "DistrictName");
            ViewBag.ActivityID = new SelectList(db.Activities, "ID", "ActivityName");
            ViewBag.PlaceTypeID = new SelectList(db.PlaceTypes, "ID", "PlaceTypeName");
            ViewBag.UserId = new SelectList(db.UserProfiles, "UserId", "UserName");
            return View();
        }

        //
        // POST: /Places/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Place place)
        {
            if (ModelState.IsValid)
            {
                db.Places.Add(place);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.DistrictID = new SelectList(db.Districts, "ID", "DistrictName", place.DistrictID);
            ViewBag.ActivityID = new SelectList(db.Activities, "ID", "ActivityName", place.ActivityID);
            ViewBag.PlaceTypeID = new SelectList(db.PlaceTypes, "ID", "PlaceTypeName", place.PlaceTypeID);
            ViewBag.UserId = new SelectList(db.UserProfiles, "UserId", "UserName", place.UserId);
            return View(place);
        }

        //
        // GET: /Places/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Place place = db.Places.Find(id);
            if (place == null)
            {
                return HttpNotFound();
            }
            ViewBag.DistrictID = new SelectList(db.Districts, "ID", "DistrictName", place.DistrictID);
            ViewBag.ActivityID = new SelectList(db.Activities, "ID", "ActivityName", place.ActivityID);
            ViewBag.PlaceTypeID = new SelectList(db.PlaceTypes, "ID", "PlaceTypeName", place.PlaceTypeID);
            ViewBag.UserId = new SelectList(db.UserProfiles, "UserId", "UserName", place.UserId);
            return View(place);
        }

        //
        // POST: /Places/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Place place)
        {
            if (ModelState.IsValid)
            {
                db.Entry(place).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.DistrictID = new SelectList(db.Districts, "ID", "DistrictName", place.DistrictID);
            ViewBag.ActivityID = new SelectList(db.Activities, "ID", "ActivityName", place.ActivityID);
            ViewBag.PlaceTypeID = new SelectList(db.PlaceTypes, "ID", "PlaceTypeName", place.PlaceTypeID);
            ViewBag.UserId = new SelectList(db.UserProfiles, "UserId", "UserName", place.UserId);
            return View(place);
        }

        //
        // GET: /Places/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Place place = db.Places.Find(id);
            if (place == null)
            {
                return HttpNotFound();
            }
            return View(place);
        }

        //
        // POST: /Places/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Place place = db.Places.Find(id);
            db.Places.Remove(place);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}