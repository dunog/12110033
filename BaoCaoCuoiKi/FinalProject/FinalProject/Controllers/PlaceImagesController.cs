﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FinalProject.Models;

namespace FinalProject.Controllers
{
    public class PlaceImagesController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /PlaceImages/

        public ActionResult Index()
        {
            var placeimages = db.PlaceImages.Include(p => p.Place);
            return View(placeimages.ToList());
        }

        //
        // GET: /PlaceImages/Details/5

        public ActionResult Details(int id = 0)
        {
            PlaceImage placeimage = db.PlaceImages.Find(id);
            if (placeimage == null)
            {
                return HttpNotFound();
            }
            return View(placeimage);
        }

        //
        // GET: /PlaceImages/Create

        public ActionResult Create()
        {
            ViewBag.PlaceID = new SelectList(db.Places, "ID", "PlaceName");
            return View();
        }

        //
        // POST: /PlaceImages/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PlaceImage placeimage)
        {
            if (ModelState.IsValid)
            {
                db.PlaceImages.Add(placeimage);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PlaceID = new SelectList(db.Places, "ID", "PlaceName", placeimage.PlaceID);
            return View(placeimage);
        }

        //
        // GET: /PlaceImages/Edit/5

        public ActionResult Edit(int id = 0)
        {
            PlaceImage placeimage = db.PlaceImages.Find(id);
            if (placeimage == null)
            {
                return HttpNotFound();
            }
            ViewBag.PlaceID = new SelectList(db.Places, "ID", "PlaceName", placeimage.PlaceID);
            return View(placeimage);
        }

        //
        // POST: /PlaceImages/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PlaceImage placeimage)
        {
            if (ModelState.IsValid)
            {
                db.Entry(placeimage).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PlaceID = new SelectList(db.Places, "ID", "PlaceName", placeimage.PlaceID);
            return View(placeimage);
        }

        //
        // GET: /PlaceImages/Delete/5

        public ActionResult Delete(int id = 0)
        {
            PlaceImage placeimage = db.PlaceImages.Find(id);
            if (placeimage == null)
            {
                return HttpNotFound();
            }
            return View(placeimage);
        }

        //
        // POST: /PlaceImages/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PlaceImage placeimage = db.PlaceImages.Find(id);
            db.PlaceImages.Remove(placeimage);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}