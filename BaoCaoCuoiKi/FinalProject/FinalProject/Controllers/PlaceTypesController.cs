﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FinalProject.Models;

namespace FinalProject.Controllers
{
    public class PlaceTypesController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /PlaceTypes/

        public ActionResult Index()
        {
            return View(db.PlaceTypes.ToList());
        }

        //
        // GET: /PlaceTypes/Details/5

        public ActionResult Details(int id = 0)
        {
            PlaceType placetype = db.PlaceTypes.Find(id);
            if (placetype == null)
            {
                return HttpNotFound();
            }
            return View(placetype);
        }

        //
        // GET: /PlaceTypes/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /PlaceTypes/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PlaceType placetype)
        {
            if (ModelState.IsValid)
            {
                db.PlaceTypes.Add(placetype);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(placetype);
        }

        //
        // GET: /PlaceTypes/Edit/5

        public ActionResult Edit(int id = 0)
        {
            PlaceType placetype = db.PlaceTypes.Find(id);
            if (placetype == null)
            {
                return HttpNotFound();
            }
            return View(placetype);
        }

        //
        // POST: /PlaceTypes/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PlaceType placetype)
        {
            if (ModelState.IsValid)
            {
                db.Entry(placetype).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(placetype);
        }

        //
        // GET: /PlaceTypes/Delete/5

        public ActionResult Delete(int id = 0)
        {
            PlaceType placetype = db.PlaceTypes.Find(id);
            if (placetype == null)
            {
                return HttpNotFound();
            }
            return View(placetype);
        }

        //
        // POST: /PlaceTypes/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PlaceType placetype = db.PlaceTypes.Find(id);
            db.PlaceTypes.Remove(placetype);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}