namespace Blog3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Tags", "Content", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Tags", "Content", c => c.String(nullable: false, maxLength: 100));
        }
    }
}
