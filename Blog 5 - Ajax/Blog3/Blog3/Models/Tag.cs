﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog3.Models
{
    public class Tag
    {
        public int ID { set; get; }
        [Required]
        //[MaxLength(100, ErrorMessage = "Độ dài lớn nhất là 100 ký tự")]
        //[MinLength(10, ErrorMessage = "Độ dài nhỏ nhất là 10 ký tự")]
        public string Content { set; get; }
        public virtual ICollection<Post> Posts { get; set; }
    }
}